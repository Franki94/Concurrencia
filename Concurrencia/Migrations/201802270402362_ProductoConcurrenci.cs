namespace Concurrencia.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ProductoConcurrenci : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.Productoes", newName: "Producto");
            AddColumn("dbo.Producto", "Version", c => c.Binary(nullable: false, fixedLength: true, timestamp: true, storeType: "rowversion"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Producto", "Version");
            RenameTable(name: "dbo.Producto", newName: "Productoes");
        }
    }
}
