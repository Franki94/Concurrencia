﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Concurrencia.Models
{
    [Table("Producto")]
    public class Producto
    {
        public int ID { get; set; }

        public string Name { get; set; }

        public double Price { get; set; }

        public int Stock { get; set; }

        [Timestamp]
        public byte[] Version { get; set; }
    }
}