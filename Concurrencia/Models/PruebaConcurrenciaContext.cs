﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace Concurrencia.Models
{
    public class PruebaConcurrenciaContext : DbContext
    {
        public PruebaConcurrenciaContext() : base("Concurencia")
        {

        }

        public DbSet<Producto> Producto { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
        }
    }
}