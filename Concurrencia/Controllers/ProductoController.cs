﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Concurrencia.Models;

namespace Concurrencia.Controllers
{
    public class ProductoController : Controller
    {
        private PruebaConcurrenciaContext db = new PruebaConcurrenciaContext();

        // GET: Producto
        public ActionResult Index()
        {
            return View(db.Producto.ToList());
        }

        // GET: Producto/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Producto.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // GET: Producto/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Producto/Create
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,Name,Price,Stock,Version")] Producto producto)
        {
            if (ModelState.IsValid)
            {
                db.Producto.Add(producto);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(producto);
        }

        // GET: Producto/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Producto.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            return View(producto);
        }

        // POST: Producto/Edit/5
        // Para protegerse de ataques de publicación excesiva, habilite las propiedades específicas a las que desea enlazarse. Para obtener 
        // más información vea https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost, ActionName("Edit")]
        [ValidateAntiForgeryToken]
        public ActionResult EditPost(int? id, byte[] version)
        {
            string[] fieldsToBind = new string[] { "Name", "Price", "Stock", "Version" };

            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var productoAActualizar = db.Producto.Find(id);

            if (productoAActualizar == null)
            {
                var productoBorrado = new Producto();
                TryUpdateModel(productoBorrado, fieldsToBind);
                ModelState.AddModelError(string.Empty,
            "El producto fue borrado por alguien mas.");
                
                return View(productoBorrado);
            }

            if (TryUpdateModel(productoAActualizar, fieldsToBind))
            {
                try
                {
                    db.Entry(productoAActualizar).OriginalValues["Version"] = version;
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch (DbUpdateConcurrencyException ex)
                {
                    var entry = ex.Entries.Single();
                    var clientValues = (Producto)entry.Entity;
                    var dataBaseEntry = entry.GetDatabaseValues();
                    if (dataBaseEntry == null)
                    {
                        ModelState.AddModelError(string.Empty,"El producto fue borrado por alguien mas.");
                    }
                    else
                    {
                        var dataBaseValues = (Producto)dataBaseEntry.ToObject();
                        if (dataBaseValues.Name != clientValues.Name)
                            ModelState.AddModelError("Name", "Valor actual: " + dataBaseValues.Name);
                        if(dataBaseValues.Price != clientValues.Price)
                            ModelState.AddModelError("Price", "Valor actual: " + dataBaseValues.Price);
                        if (dataBaseValues.Stock != clientValues.Stock)
                            ModelState.AddModelError("Stock", "Valor actual: " + dataBaseValues.Stock);

                        ModelState.AddModelError(string.Empty, "El registro fue modificado por otro usuario, si quiere continuar precione SAVE otra vez, si no vuelva al index");

                        productoAActualizar.Version = dataBaseValues.Version;
                    }
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Unable to save changes. Try again, and if the problem persists, see your system administrator.");
                }

            }
            return View(productoAActualizar);
        }

        // GET: Producto/Delete/5
        public ActionResult Delete(int? id, bool? ocurrioError)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Producto producto = db.Producto.Find(id);
            if (producto == null)
            {
                return HttpNotFound();
            }
            if (ocurrioError.GetValueOrDefault())
            {
                ViewBag.ErrorMessage = "El registro fue modificado, desde que accediste a el, si aun asi quieres borrarlo continua, sino da vuelta atras";
            }
            return View(producto);
        }

        // POST: Producto/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(Producto producto)
        {
            try
            {
                db.Entry(producto).State = EntityState.Deleted;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            catch (DbUpdateConcurrencyException)
            {
                return RedirectToAction("Delete", new { ocurrioError = true, id = producto.ID });
            }
            catch (DataException)
            {                
                ModelState.AddModelError(string.Empty, "Unable to delete. Try again, and if the problem persists contact your system administrator.");
                return View(producto);
            }
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
